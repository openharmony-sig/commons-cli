# commons-cli

## 项目简介

commons-cli 是一个命令行解析工具，它可以帮助开发者快速构建启动命令，并且帮助组织命令的参数、以及输出列表等。

## 效果展示：

![gif](preview.gif)

## 下载安装

 ```
 ohpm install @ohos/commons-cli
 ```

OpenHarmony ohpm环境配置等更多内容，请参照 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明

第一步： 初始化 Options
```
let optionsmy = new Options();
```
第二步：添加 Option 到 Options 中

```
optionsmy.addOption(Option.createOption("a", false, "do not hide entries starting with .", "all"));
```

第二步：初始化解析工具，开始解析

```
let parsermy: CommandLineParser = new DefaultParser();
let cmdmy: CommandLine = parsermy.parse(optionsmy, args, null, false);
console.log("commons-cli cmdmy.hasOption:" + cmdmy.hasOption("block-size"));
console.log("commons-cli line.getOptionValue:" + cmdmy.getOptionValue("block-size"));
```

更多详细用法请参考开源库sample页面的实现

## 接口说明
```
let option = Option.build();
let option = Option.createOption();
```
1. 设置此选项的长名称：option.buildLongOpt()
2. 获取此选项的自文档说明：option.setDescription()
3. 设置此选项是否可以有可选参数：option.setOptionalArg()
4. 设置此选项可以接受的参数值的数量：option.setArgs()
```
   let optionsmy = new Options();
```
1. 添加一个选项实例：optionsmy.addOption()
2. 添加一个选项组：optionsmy.addOptionGroup()
3. 判断是否存在选项：optionsmy.hasOption()
4. 获得选项所在的选项组：optionsmy.getOptionGroup()
5. 获得一个只读的选项集合：optionsmy.getOptions()
6. 判断是否存在选项：optionsmy.hasLongOption()

```
let parsermy: CommandLineParser = new DefaultParser();
```
1. 根据指定的选项和属性分析参数：parsermy.parse()
2. 判断是否含有选项：parsermy.hasOption()
3. 获得选项值：parsermy.getOptionValue()
   .......

## 约束与限制

在下述版本验证通过：

- DevEco Studio 版本： 4.1 Canary(4.1.3.317)

- OpenHarmony SDK:API11 (4.1.0.36)

## 目录结构

```
|---- commons_cli  
|     |---- entry  # 示例代码文件夹
|     |---- library  # commons_cli库文件夹
|          |---- src
|            |---- main
|              |---- ets
|                  |---- commonents
|                    |---- cli     # 核心库代码文件夹
|                      |---- DefaultParser.ets     # 默认解析API
|                      |---- Option.ets            # 选项配置
|                      |---- Options.ets           # 多个选项配置
|                      |---- OptionGroup.ets       # 选项组配置
|                      |---- HelpFormatter.ets     # 格式化帮助类
|                      ......
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法
|     |---- README_zh.md  # 安装使用方法
```

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/commons-cli/issues) ，当然，非常欢迎发 [PR](https://gitee.com/openharmony-sig/commons-cli/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/commons-cli/blob/master/LICENSE) ，请自由地享受和参与开源。


