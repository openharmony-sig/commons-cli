## v2.0.1

- Chore:Added supported device types

## v2.0.0

- Compatible with DevEco Studio version: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API11 (4.1.0.36)
- New grammar adaptation

## v1.0.3

- Compatible with DevEco Studio version: 3.1 Beta1 (3.1.0.200), OpenHarmony SDK: API9 (3.2.10.6)

## v1.0.2

- Upgrade from API8 to API9 stage model

## v1.0.0

- Implemented functions
  1、Definition phase: Define Optin parameters in Java code, including parameters, whether input values are required, and a brief description
  2、Analysis phase: After the application passes in parameters, CLI performs parsing
  3、Inquiry stage: Inquire which program branch to enter by querying CommandLine