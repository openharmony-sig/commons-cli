# commons-cli

## Introduction

commons-cli is a command line parsing tool. It helps developers quickly build startup commands and organize command parameters and output lists.

## Display Effects

![gif](preview.gif)

## How to Install

 ```
 ohpm install @ohos/commons-cli
 ```

For details, see [Installing an OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

Step 1: Initialize **Options**.
```
let optionsmy = new Options();
```
Step 2: Add an option to **Options**.

```
optionsmy.addOption(Option.createOption("a", false, "do not hide entries starting with .", "all"));
```

Step 2: Initialize the parsing tool and start parsing.

```
let parsermy: CommandLineParser = new DefaultParser();
let cmdmy: CommandLine = parsermy.parse(optionsmy, args, null, false);
console.log("commons-cli cmdmy.hasOption:" + cmdmy.hasOption("block-size"));
console.log("commons-cli line.getOptionValue:" + cmdmy.getOptionValue("block-size"));
```

For details, see the implementation on the sample page of the open-source library.

## Available APIs
```
let option = Option.build();
let option = Option.createOption();
```
1. option.buildLongOpt(): Sets the long name of this option.
2. option.setDescription(): Obtains the self-document description of this option.
3. option.setOptionalArg(): Sets whether the option has optional parameters.
4. option.setArgs(): Sets the number of parameter values allowed by this option
```
   let optionsmy = new Options();
```
1. optionsmy.addOption(): Adds an option instance.
2. optionsmy.addOptionGroup(): Adds an option group.
3. optionsmy.hasOption(): Checks whether an option exists.
4. optionsmy.getOptionGroup(): Obtains the group to which an option belongs.
5. optionsmy.getOptions(): Obtains a read-only option set.
6. optionsmy.hasLongOption(): Checks whether a long option exists.

```
let parsermy: CommandLineParser = new DefaultParser();
```
1. parsermy.parse(): Analyzes parameters based on specified options and attributes.
2. parsermy.hasOption(): Checks whether an option exists.
3. parsermy.getOptionValue(): Obtains the option value.
   .......

## Constraints

This project has been verified in the following version:

- DevEco Studio: 4.1 Canary (4.1.3.317)

- OpenHarmony SDK:API11 (4.1.0.36)

## Directory Structure

```
|---- commons_cli  
|     |---- entry  # Sample code
|     |---- library  # commons_cli library
|          |---- src
|            |---- main
|              |---- ets
|                  |---- commonents
|                    |---- cli     # Core library
|                      |---- DefaultParser.ets     # Default parsing API
|                      |---- Option.ets            # Option configuration
|                      |---- Options.ets           # Configuration of multiple options
|                      |---- OptionGroup.ets       # Option group configuration
|                      |---- HelpFormatter.ets     # Formatting help class
|                      ......
|           |---- index.ets  # External APIs
|     |---- README.md  # Readme
|     |---- README_zh.md  # Readme
```

## How to Contribute
If you find any problem during the use, submit an [Issue](https://gitee.com/openharmony-sig/commons-cli/issues) or [PR](https://gitee.com/openharmony-sig/commons-cli/pulls) to us.

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/commons-cli/blob/master/LICENSE).
